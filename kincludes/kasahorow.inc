<?php

/**
 * @file
 * This file is needed by all the kasahorow modules. It contains
 * the definitions of all the common functions that may be used
 * by any kasahorow module
 */
 
define('KASAHOROW_PATH', drupal_get_path('module', 'kasahorow'));
_kasahorow_include_configured();
 
 /**
 * The main kasahorow control panel
 */
function _kw_admin_home(){
	return theme('kw_cpanel');
} 

function _cpanel_version(){
	return "<div align='right'><img src='http://kasahorow.com/logo.png' /><br />".t('Version 0.4')."</div>";
}

/**
 * The kasahorow control panel
 */
function theme_kw_cpanel(){
	$output.= _cpanel_version();
	
	$output.= kasahorow_help('admin/cpanel');
	
	$kasa = (object)$kasa;
	$header = array(t('Module'), t('Description'), '');
	
	//Show what modules are available, and which languages they
	//implement functionality for
	$admins = kasahorow_invoke_kwapi($kasa, 'admin');	
	foreach($admins as $key=>$value){
		$rows[$key] = $value;
	}
	if(count($rows) < 1){
		$rows[] = array(array('data'=>t('No modules have been enabled'), 'colspan'=>3));
	}	
	$output.=theme('table', $header, $rows, array('width'=>'100%'));	
	return $output;
}
 /**
 * Substitute $char with $subst in $word one position at
 * a time to generate a new word. Recurse till all possible
 * new words have been obtained
 */
function _alt_words($word, $char, $subst, $pos=0, &$com)
{print $word.$char."|";
	static $words = array();
	
	if($pos < drupal_strlen($word))
	{
		$posn = mb_strpos($word, $char, $pos);
		
		if(is_numeric($posn))//$posn may well be 0
		{
			
			if($pos <= $posn)
			{
			//	$word{$posn} = $subst;
				$words[] = preg_replace("/$char/",$subst, $word, 1);
				
				_alt_words($words[$pos], $char, $subst, $posn+1);
				return;
			}
			print_r($words);
		//	exit();
		}
	}
}

/**
 * from http://us2.php.net/array_walk
 */
function _string_walk(&$string, $funcname, $userdata = null) {
   for($i = 0; $i < strlen($string); $i++) {
       # NOTE: PHP's dereference sucks, we have to do this.
       $hack = $string{$i};
       call_user_func($funcname, $hack, $i, $userdata);
       $string{$i} = $hack;
   }
}
 




/**
 * Badly named! Thought it was a theme function
 */
function _kasahorow_page($keys, $lang="all"){
	$lang = ($lang=="all")?$_GET['lang']:"all";
	
	if($lang!='all')
	{
		$q = '+type:kentry_'.$lang;
	}
	else
	{
		$q = '';
	}
	
	$dictionarylink = variable_get('kasahorow_dictlink', 'http://dictionary.kasahorow.com?q=search/node/');
	//header("Location: ".$dictionarylink.$keys.$q);
} 



function _tag_links($matches)
{
	//print_r($matches);
	$target = (variable_get('kasahorow_target', 1)==1)?array('target'=>"kasahorowdictionary", 'title'=>t('View dictionary meaning')):array();
	return l($matches[2], 'kasahorowfilter', $target, 'keys='.check_plain($matches[2]).'&'.trim(check_plain(preg_replace(array('|"|', "|'|"), '', $matches[1]))));
}

/**
 * Returns an associative array of langs currently supported
 * by the kasahorow system where
 *   -> the keys represent the two-letter ISO tag and
 *   -> the values represent the English name of the lang.
 * The array is sorted the index (i.e., by the short English name of the lang).
 *
 * Please note the different between "supported" langs and "configured"
 * langs: A "supported" language means that the language has been added via the locale module. 
 * A "configured" language means that the site admin has configured the site to actually use 
 * features for that language. 
 * If $all is set, return all Drupal languages
 * If $exclude, exclude supported langs
 */
function _kasahorow_supported_langs($all=FALSE, $includenotadded=TRUE) {
	//$supported = locale_supported_languages(TRUE, TRUE);
	$supported = _kasahorow_langs(); 
  /*	if(module_exist('locale')){
  		if($all){
  			 include_once './includes/locale.inc';  			 
  			 if($includenotadded){
  			 	$supported += _locale_prepare_iso_list();
  			 }else{
  			 	$supported = _locale_prepare_iso_list();
  			 }   			 		 
  		}
  	}else{
  		drupal_set_message(t("The locale module is required to list ISO Codes. Please enable it"), 'warning');
  	}*/
  return $supported;
	
}


/**
 * This function simply loads the include file for each lang whose
 * languages are configured to be recognized by the kasahorow system.
 */
function _kasahorow_include_configured() {
  /*$configured_lang_tags = array_keys(_kasahorow_configured_langs());
  foreach ($configured_lang_tags as $lang_tag) {
    $filename = KASAHOROW_PATH.'/supported/kasahorow.'. $lang_tag .'.inc';
    if (file_exists($filename)) {
      include_once($filename);
    }
  }*/
}

?>
<?php //$Id
/**
 * Auxiliary file for kasahorow.module
 */
 
define('KASAHOROW_PATH', drupal_get_path('module', 'kasahorow'));


/**
 * Tokenizer. Split input text into tokens based on type
 * of segmentation requested
 * @args $input_text, $segment_type
 * 
 */
function _ktokenize($input_text, $segment_type){
	$input_text = strip_tags($input_text);
	switch($segment_type){
		case 'sentence':
		$output = preg_split("/\./",$input_text );
		break;
		case 'word':
		$output = preg_split("/\s/",$input_text );
		//$output = preg_split("[ \\s0-9\\.;/:,\"!_\\-<>()\\?\\[\\]\\*â€¦&]", $input_text);
		break;
	}
	return $output;
}

/**
 * Return iso of suspected language text
 * Implemented with info from http://en.wikipedia.org/wiki/Language_recognition_chart
 */
function _kguess_lang($text){
	if(strstr($text, 'kwenye')){return 'sw';}
	else return '?';
}

/**
 * Return all languages listed by the locale module, enabled or not. 
 * If the iso is specified, return only that language's name
 * @args
 * $iso - the ISO language code
 * $enabled - whether this language is enabled in the global settings
 */
function _kasahorow_langs($iso=NULL, $enabled=FALSE){
  	//check for existence of locale module
  	if(module_exist('locale')){
	  	$supported = (locale_supported_languages(FALSE, TRUE));
		$supported = $supported['name'];
		include_once './includes/locale.inc';
		$supported += _locale_prepare_iso_list();
		if(!$enabled){	
			return isset($iso) ? $supported[$iso] : $supported;
		}else{ 
			return _kasahorow_enabled_langs();
		}	  	
  	}else{
  		form_set_error( 'kasahorow', t("The locale module is required to list ISO Codes. Please enable it"));
  		return array();
  	}
}




/**
 * Implementation of kasahorow text filters
 * @arguments
 * $text - text to be filtered
 * $format - input format selected
 * $type - 'full' (full text filtering) or 'tags' (tagged text filtering)
 * @return
 * filtered text
 */
function _kw_filter($text, $format, $type='full')
{
	switch($type)
	{
		case 'full': //replace each word which is not a url with a link to kasahorowfilter
	  	$target = (variable_get('kasahorow_target', 1)==1)?array('target'=>"kasahorowdictionary", 'title'=>t('View dictionary meaning')):array();
   		$specialchars = variable_get('kasahorow_schars', 'ɛƐɔƆ');
   		
   		mb_regex_encoding("utf-8");
	    
	   /* $regex = '(<.+?>)|([\w][a-z'.$specialchars.']{1,})'; //pull out words that start with a word character followed by 1 or more lower case characters
	    * 
	    */ 
	    $ignore = array('\w&.*;\b'); //regex array of entities to ignore
	    //TODO: include ignore array in regex
	    $regex = '(<.+?>)|([\w'.$specialchars.'][a-z'.$specialchars.']{1,})'; //pull out words that start with a word character followed by 1 or more lower case characters
	    $words = array();
	    preg_match_all("/".$regex."/", $text, $words);
		$words = $words[2];
			foreach($words as $word)
			{
				$replacements[$word] = ($word=="")?"": l($word, 'kasahorowfilter', $target, 'keys='.$word);
			}		
			$text = preg_replace("/$regex/e", ' ( empty($replacements["$2"]) ? "$1$2" : "$1".$replacements["$2"] ) ', $text);	
		break;
		case 'tags':		
		$text = filter_xss($text, array('kw')); 
		$text = preg_replace_callback("|<kw(.*)>(.*)</kw>|", '_tag_links', $text);
		break;
	}
	return $text;
}



/**
 * Fetch all enabled languages. Requires the locale module
 *
function _kasahorow_langs()
{
		//check for existence of locale module
  	if(module_exist('locale'))
  	{
  	$languages=locale_supported_languages(TRUE, TRUE); 
  	$isotags = _get_iso_tag();
  	//check for a non-empty array first
  	if(!empty($isotags))
  	{
	  	foreach ($isotags as $key => $value) {
	      if (isset($languages['name'][$key])) {
	        unset($isotags[$key]);
	        continue;
	      }
	      if (count($value) == 2) {
	        $tname = t($value[0]);
	        $isotags[$key] = ($tname == $value[1]) ? $tname : "$tname ($value[1])";
	      }
	      else {
	        $isotags[$key] = t($value[0]);
	      }
	    }
	    $isotags = array_merge($isotags, $languages['name']);
	    asort($isotags); 
	    return $isotags;
  	}
  	}
  	else
  	{
  		print theme('warning', "The locale module is required to list ISO Codes. Please enable it");
  	}
}*/

function _kasahorow_configure(){	
	$form = kasahorow_settings();
  	return system_settings_form('kasahorow_configure_settings', $form);
}

/**
 * The language options page allows you to specify basic language-specific features 
 * for the languages that are available.
 */
function _language_options_page(){
	$form['kasahorow_lang_options'] = array('#type'=> 'fieldset', '#title' => t('Language Options'), '#tree'=>TRUE,
											'#description'=>t('Configure basic options for each language you selected from Global Settings.'));
	foreach(_kasahorow_enabled_langs() as $tag=>$language){ 
		$vars = variable_get('kasahorow_lang_options', array(
												   $tag => array('dict'=>array('link' =>'http://dictionary.kasahorow.com?q=search/node/type:kentry_'.$tag.' ',
																 'blurb' => "Dictionary search powered by <a href='http://dictionary.kasahorow.com'>kasahorow</a>",
																 'target'=> 1,
																 )),
																 'node'=>array('tid' => 0)
																 
															 )
							);
							
		$form['kasahorow_lang_options'][$tag] = array('#type'=> 'fieldset', '#title' => ($language), '#tree'=>TRUE, '#collapsible'=>TRUE, '#collapsed'=>TRUE);		
		
		$form['kasahorow_lang_options'][$tag]['node'] = array('#type'=> 'fieldset', '#title' => t('Taxonomy Settings'), '#collapsible'=>TRUE, '#collapsed'=>TRUE);		
		$form['kasahorow_lang_options'][$tag]['node']['tid']= _taxonomy_term_select(t('Filed under'),'tid',  $vars[$tag]['node']['tid'], variable_get('kw_lang_vid', 0), 
										t('Sub-categories of languages are not shown because they are categorized with the same language code. ').t('Browse by language. ').l(t('Modify this list'), 'admin/kw/settings'), '', '-'.t('none').'-' );
		
		$form['kasahorow_lang_options'][$tag]['dict'] = array('#type'=> 'fieldset', '#title' => t('Dictionary Settings'), '#tree'=>TRUE, '#collapsible'=>TRUE, '#collapsed'=>TRUE);
		$form['kasahorow_lang_options'][$tag]['dict']['link'] = array(
		'#title'=> t('Dictionary Search URL'), 
		'#type' => 'textfield',
		'#default_value' =>    $vars[$tag]['dict']['link'], 
		'#width' => 20, 
		'#size' => 50,
		'#description' => t('Link to dictionary search query. The word will be simply attached to this url so that on clicking, the user goes to a page where' .
				' this word has automatically been searched for.'),
		);
		$form['kasahorow_lang_options'][$tag]['dict']['blurb'] = array(
		'#title'=> t('Dictionary Attribution'), 
		'#type' => 'textarea',
		'#default_value' =>    $vars[$tag]['dict']['blurb'], 
		'#width' => 20, 
		'#rows' => 5,
		'#description' => t('Appears in the links of each page.'),
		);
		
	$form['kasahorow_lang_options'][$tag]['dict']['target'] = array(
		'#title' => t('Open dictionary in new window?'),
		'#type' => 'checkbox',
		'#default_value' => $vars[$tag]['dict']['target'],
		);	
	}			
	return system_settings_form('kasahorow_main', $form);
}



/**
 * Helper function for _kasahorow_langs(). Return enabled languages only
 * @return
 * An associative array of iso=>language name pairs
 */
function _kasahorow_enabled_langs(){
	$configured_langs = variable_get('kasahorow_cfgd_langs', array());	
	$configured_langs = is_array($configured_langs) ? $configured_langs : array('en' => t('English'));
	$return = array();  
	$vid = variable_get('kw_lang_vid', 0);  
  	foreach ($configured_langs as $tag => $lang) {
	    if ($lang!='0') {//disabled languages have a value of 0
	      $return[$tag] = _lang($lang);
	      $vars = variable_get('kasahorow_lang_options', array());
	      if(NULL ==_get_iso_from_taxonomy(array($vars[$tag]['node']['tid']=>$vars[$tag]['node']['tid']))){
	      	drupal_set_message(t("Hey, you haven't set the language category for %lang yet. You might want to set it up now under Taxonomy Settings on the Language Options page.", array('%lang'=>_lang($tag))));
	      	$possibles = (taxonomy_get_term_by_name(_lang($tag)));
	      	$found = FALSE;
	      	foreach($possibles as $term){
	      		if($term->vid == $vid){
	      			$found = TRUE;
	      			break;
	      		}	      		
	      	}
	      	
	      	if(count($possibles) > 0 && $found){
	      	//Do nothing. Life is good
	      	}else{//Create a new term for this language	      		
	      		if($vid >0){
	      			$edit['vid']= $vid;
	      			$edit['name'] = _lang($tag);
	      			taxonomy_save_term($edit);
	      		}
	      	}
	      }
	    }
	  }
	  return $return;  
}

function _get_iso_from_taxonomy($term_obj_array){
	$vars = variable_get('kasahorow_lang_options', array()); 
	foreach($vars as $iso=>$var){		
		if(in_array($var['node']['tid'], array_keys($term_obj_array))){
			return $iso;
		}
	}
	return NULL;
}

/**
 * Define a hook_kwapi() operation which can be invoked in all modules.
 *
 * @param &$kasa
 *   A language object. Properties/functionality available to the language are loaded into this object
 * @param $op
 *   A string containing the name of the kwapi operation.
 * @param $a3, $a4
 *   Arguments to pass on to the hook, after the $kasa and $op arguments.
 * @return
 *   The returned value of the invoked hooks.
 */
function kasahorow_invoke_kwapi(&$kasa, $op, $a3 = NULL, $a4 = NULL) {
  $return = array();
  foreach (module_implements('kwapi') as $name) {
    $function = $name .'_kwapi';
    $result = $function($kasa, $op, $a3, $a4);
    if (isset($result) && is_array($result)) {
      $return = array_merge($return, $result);
    }
    else if (isset($result)) {
      $return[] = $result;
    }
  }
  return $return;
}

/**
 * Return the localized name of the language in the current locale
 */
function _lang($iso){
	  return t(_kasahorow_langs($iso));		
}


/**
 * Return the kasahorow profile of this uid
 */
function _kw_account_name($uid){ 
	$account = user_load(array('uid'=>$uid));
	$fields = (variable_get('kw_account_profile', array()));
	$extra = array();		
	foreach($fields as $field){
		if($field!=''){
			$extra[]= $account->$field;
		}
	}
	$extra = join($extra, ', ');
	return $extra;
}


/**
 * array_slice with preserve_keys for every php version
 *
 * @param array $array Input array
 * @param int $offset Start offset
 * @param int $length Length
 * @return array
 */
function array_slice_preserve_keys($array, $offset, $length = null)
{
   // PHP >= 5.0.2 is able to do this itself
   if((int)str_replace('.', '', phpversion()) >= 502)
       return(array_slice($array, $offset, $length, true));

   // prepare input variables
   $result = array();
   $i = 0;
   if($offset < 0)
       $offset = count($array) + $offset- $length+1;
   if($offset<0)
	$offset = 0;
   if($length > 0)
       $endOffset = $offset + $length;
   else if($length < 0)
       $endOffset = count($array) + $length;
   else
       $endOffset = count($array);
  
   // collect elements
   foreach($array as $key=>$value)
   {
       if($i >= $offset && $i < $endOffset)
           $result[$key] = $value;
       $i++;
   }
   // return
   return($result);
}

/**
 * Return all installed vocabularies
 */
function _get_vocabularies()
{
	$rows = array('-'.t('none').'-');
	$q = db_query("SELECT vid, name FROM {vocabulary}");
	while($row = db_fetch_object($q))
	{
		$rows[$row->vid] = $row->name;
	}
	return $rows;
}

?>